modules = $(shell find * -name '*.scm' | grep -v '#')

none:
	echo $(modules) | tr ' ' '\n'
	@printf ''

game.wasm: game.scm $(modules)
	GUILE_AUTO_COMPILE=0 time guild compile-wasm --emit-names -L modules -o $@ $<

serve: game.wasm
	guile -c '((@ (hoot web-server) serve))'

bundle: game.wasm
	rm game.zip || true
	zip game.zip -r assets/ js-runtime/ game.js game.css game.wasm index.html

# test.wasm: test.scm
#	guild compile-wasm -L modules -o $@ $<

.PHONY: tests
tests: $(modules)
	guile --no-auto-compile -L modules tests.scm

clean:
	rm -f game.wasm game.zip
