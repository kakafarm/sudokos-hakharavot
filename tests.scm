;;; Sudokos Hakharavot - A sudoku game about a monster playing sudoku.
;;; Copyright (C) 2024  Yuval Langer <yuval.langer@gmail.com>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(import
 (ice-9 match)
 (ice-9 pretty-print)

 (hoot compile)
 (hoot reflect))

(define (hoot-list->list lst)
  ;; (pretty-print lst)
  (cond
   ((null? lst) '())
   ((hoot-pair? lst)
    (cons (hoot-list->list (hoot-pair-car lst))
          (hoot-list->list (hoot-pair-cdr lst))))
   ((number? lst)
    lst)
   ((hoot-keyword? lst)
    ;; (pretty-print (list 'keyword lst))
    lst)
   ((mutable-hoot-pair? lst)
    ;; (pretty-print (list 'mutable-pair lst))
    (cons (hoot-list->list (hoot-pair-car lst))
          (hoot-list->list (hoot-pair-cdr lst))))
   ;; ((hoot-object? lst)
   ;;  (pretty-print (list 'object lst))
   ;;  lst)
   ((boolean? lst)
    lst)
   ((number? lst)
    lst)
   (else
    (string->symbol (hoot-symbol-name lst)))))

(define tests-list ;; '(list (list 'FAIL))
  '(let ((solved-sudoku-board-1 (vector
                                 (vector 1 2 3 4 5 6 7 8 9)
                                 (vector 4 5 6 7 8 9 1 2 3)
                                 (vector 7 8 9 1 2 3 4 5 6)
                                 (vector 2 3 4 5 6 7 8 9 1)
                                 (vector 5 6 7 8 9 1 2 3 4)
                                 (vector 8 9 1 2 3 4 5 6 7)
                                 (vector 3 4 5 6 7 8 9 1 2)
                                 (vector 6 7 8 9 1 2 3 4 5)
                                 (vector 9 1 2 3 4 5 6 7 8)))
         (solved-sudoku-board-2 (vector
                                 (vector 1 2 3 4 5 6 7 8 9)
                                 (vector 4 5 6 7 8 9 1 2 3)
                                 (vector 7 8 9 1 2 3 4 5 6)
                                 (vector 2 3 4 5 6 7 8 9 1)
                                 (vector 5 6 7 8 9 1 2 3 4)
                                 (vector 8 9 1 2 3 4 5 6 7)
                                 (vector 3 4 5 6 7 8 9 1 2)
                                 (vector 6 7 8 9 1 2 3 4 5)
                                 (vector 9 1 2 3 4 5 6 7 8)))
         (index->t (lambda (number index)
                     (* 1.0
                        (/ index
                           (- number
                              1))))))
     (append
      (list (test-equal 1 1)

            (test-equal '(() ())
              (list-split '() 0))

            (test-equal '(() ())
              (list-split '() 0))

            (test-equal '(() (1))
              (list-split '(1) 0))

            (test-equal '((1) ())
              (list-split '(1) 1))

            (test-equal '(() (1 2 3))
              (list-split '(1 2 3) 0))

            (test-equal '((1) (2 3))
              (list-split '(1 2 3) 1))

            (test-equal '((1 2) (3))
              (list-split '(1 2 3) 2))

            (test-equal '((1 2 3) ())
              (list-split '(1 2 3) 3))

            (test-equal   '()
              (list-merge '()
                          '()
                          <))

            (test-equal   '(1 2 3 4 5   7 8 9)
              (list-merge '(1     4 5     8 9)
                          '(  2 3       7)
                          <))

            (test-equal #t (member-less? 1 2))
            (test-equal #f (member-less? 1 1))
            (test-equal #f (member-less? #f #f))
            (test-equal #f (member-less? #f #f))
            (test-equal #t (member-less? 1 2))

            (test-equal '()
              (list-merge-sort '() <))

            (test-equal '(1 2 3 4 5 7 8 9)
              (list-merge-sort '(5 9 4 2 7 1 3 8) <))

            (test-equal '(#f #f 3 4 5 7 8 9)
              (sort-members '(5 9 4 #f 7 #f 3 8)))

            (test-equal #f
              (solved-members? '()))

            (test-equal #f
              (solved-members? '(5 9 4 #f 7 #f 3 8)))

            (test-equal #t
              (solved-members? '(1 2 3 4 5 6 7 8 9)))

            (test-equal '(1 2 3 4 5 6 7 8 9)
              (members-of-sub-board solved-sudoku-board-1 0 3 0 3))

            (test-equal #t
              (sudoku-blocks-solved? solved-sudoku-board-1))

            (test-equal #t
              (sudoku-solved? solved-sudoku-board-1))

            (test-approximate 0.0 0.0 0.1)
            (test-approximate 0.0 0.1 0.1)
            (test-approximate (* 0.0) (* 0.2) (* 0.1)))

      (let ((number 4))
        (map (lambda (i t)
               (test-approximate t
                 (index->t number
                           i)
                 0.001))
             '(0 1 2 3)
             '(0.0 0.3333 0.66666 1.0)))

      (map (lambda (l t)
             (test-approximate l
               (lerp -0.2 0.2 t)
               0.0001))
           '(-0.2 -0.1 0.0 0.1 0.2)
           '(0.0 0.25 0.5 0.75 1.0))

      (let ((W 680.0)
            (N 9.0))
        (map (lambda (x)
               (test-approximate W
                 (+ (* (+ N 1)
                       (lerp 0.0
                             (/ W (+ N 1))
                             x))
                    (* N
                       (lerp (/ W N)
                             0.0
                             x)))
                 0.00001))
             '(0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0)))

      )))

(define (test-failed? test)
  (eq? (car test)
       'FAIL))

((compose (lambda (x) (cond ((null? x) (display "ALL PASS") (newline))
                            (else (pretty-print x))))
          ;; (lambda (x) (filter test-failed? x))
          hoot-list->list
          ;; compile-value
          hoot-load
          hoot-instantiate
          )
 ((@ (hoot compile) compile)
  tests-list
  #:imports
  '((scheme base)
    (scheme write)

    (hoot debug)
    (hoot ffi)
    (hoot hashtables)
    (hoot match)

    (game checker utils)
    (game tests utils)
    (game utils)
    )
  #:extend-load-library
  (library-load-path-extension '("modules/"))))
