#!/bin/sh

while sleep 0.1; do
    make clean
    {
        find . -name '*.scm' -exec printf "%s\n" {} \; \
             , -name '*.js' -exec printf "%s\n" {} \;
        printf "Makefile
serve.sh
"
    } | entr -r guix shell -CN -m manifest.scm -- make serve
done
