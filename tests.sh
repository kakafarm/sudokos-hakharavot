#!/bin/sh

while sleep 0.1; do
    find . \
         -name '*.scm' \
         -o \
         -name '*.js' |
        entr -s 'guix shell -C -m manifest.scm -- make tests'
    # entr -r guix shell -CN -m manifest.scm -- make serve
done
