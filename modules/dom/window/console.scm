;;; Copyright (C) 2024 David Thompson <dave@spritely.institute>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Sudokos Hakharavot - A sudoku game about a monster playing sudoku.
;;; Copyright (C) 2024  Yuval Langer <yuval.langer@gmail.com>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;;
;;; Console bindings.
;;;
;;; Code:

(define-module (dom window console)
  #:pure
  #:use-module (scheme base)
  #:use-module (hoot ffi)
  #:export (console:debug
            console:error
            console:info
            console:log
            console:warn))

(define-foreign console:debug "console" "debug" (ref string) -> none)
(define-foreign console:error "console" "error" (ref string) -> none)
(define-foreign console:info "console" "info" (ref string) -> none)
(define-foreign console:log "console" "log" (ref string) -> none)
(define-foreign console:warn "console" "warn" (ref string) -> none)
