(define-module (game utils)
  :pure
  #:export (lerp)
  #:use-module (scheme base)
  #:use-module (scheme write))

(define (lerp from to t)
  (+ (* (- 1.0 t) from)
     (* t to)))
