(define-module (game checker)
  #:pure
  ;; #:export (sudoku-solved?)
  #:use-module (scheme base)
  #:use-module (game checker utils)
  #:use-module (game utils))
