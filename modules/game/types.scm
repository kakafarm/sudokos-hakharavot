(define-module (game utils)
  :pure
  #:export (
            make-game
            game-width
            game-height
            game-board
            game-state

            make-new-board
            board-ref
            )
  #:use-module (scheme base)
  #:use-module (scheme write))

(define-record-type <game>
  (make-game width height board game-state)
  game?
  (board game-board)
  (width game-width)
  (height game-height)
  ;; Two states:
  ;; 'sudoku-board
  ;; 'digits
  (state game-state set-game-state!)
  )

(define (make-new-board)
  (make-vector (* 9 9) #f))

(define (board-ref board row-index column-index)
  (vector-ref board (+ column-index
                       (* row-index 9))))
