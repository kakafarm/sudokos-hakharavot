(define-module (game checker utils)
  #:pure
  #:export (list-split
            member-less?
            list-merge
            list-merge-sort
            sort-members
            solved-members?
            sudoku-blocks-solved?
            members-of-sub-board
            sudoku-solved?
            )
  #:use-module (scheme base)
  #:use-module (hoot match)
  ;; #:use-module (game utils)
  )

(define (list-split xs split-index)
  (let loop ((current-index 0)
             (left '())
             (right xs))
    (cond
     ((>= current-index split-index)
      (list (reverse left)
            right))
     (else
      (loop (+ current-index 1)
            (cons (car right)
                  left)
            (cdr right))))))

(define (list-merge xxs yys less?)
  (match `(,xxs ,yys)
    ((() yys)
     yys)
    ((xxs ())
     xxs)
    (((x . xs) (y . ys))
     (cond
      ((less? x y)
       (cons x (list-merge xs yys less?)))
      (else
       (cons y (list-merge xxs ys less?)))))))

(define (list-merge-sort xs less?)
  (match xs
    (() '())
    ((x) xs)
    (otherwise
     (match (list-split xs
                        (quotient (length xs)
                                  2))
       ((left right)
        (list-merge (list-merge-sort left less?)
                    (list-merge-sort right less?)
                    less?))))))

(define (list-sort lst less?)
  "Return LST sorted according to the procedure less?."
  (list-merge-sort lst less?))

(define (member-less? a b)
  (match (list a b)
    ((#f #f) #f)
    ((#f b) #t)
    ((a #f) #f)
    ((a b) (< a b))))

(define (sort-members members)
  (list-sort members member-less?))

(define (solved-members? members)
  (equal? members
          '(1 2 3 4 5 6 7 8 9)))

(define (sudoku-blocks-solved? board)
  (let loop ((current-block-row-index 0)
             (current-block-column-index 0))
    (cond
     ((>= current-block-row-index 3)
      #t)
     ((>= current-block-column-index 3)
      (loop (+ current-block-row-index 1)
            0))
     (else
      (let* ((current-members (members-of-block board
                                                current-block-row-index
                                                current-block-column-index))
             (solved (solved-members? current-members)))
        (if solved
            (loop current-block-row-index
                  (+ current-block-column-index 1))
            #f))))))

(define (sudoku-rows-solved? board)
  (let loop ((current-row-index 0))
    (cond
     ((< current-row-index 9)
      (let* ((current-members (members-of-row board
                                              current-row-index))
             (current-row-solved (solved-members? current-members)))
        (if current-row-solved
            (loop (+ current-row-index 1))
            #f)))
     (else
      #t))))

(define (sudoku-columns-solved? board)
  (let loop ((current-column-index 0))
    (cond
     ((< current-column-index 9)
      (let* ((current-members (members-of-column board
                                                 current-column-index))
             (current-column-solved (solved-members? current-members)))
        (if current-column-solved
            (loop (+ current-column-index 1))
            #f)))
     (else
      #t))))

(define (members-of-sub-board board from-row to-row from-column to-column)
  (let loop ((current-row-index from-row)
             (current-column-index from-column)
             (members '()))
    (cond
     ((>= current-row-index to-row)
      (sort-members members))
     ((>= current-column-index to-column)
      (loop (+ current-row-index 1)
            from-column
            members))
     (else
      (let* ((current-row (vector-ref board current-row-index))
             (current-element (vector-ref current-row current-column-index))
             (new-members (cons current-element
                                members)))
        (loop current-row-index
              (+ current-column-index 1)
              new-members))))))


(define (members-of-block board block-row-index block-column-index)
  "Return the members of the BOARD at the BLOCK-ROW-INDEX and BLOCK-COLUMN-INDEX block."
  (unless (and (<= 0 block-row-index 3)
               (<= 0 block-column-index 3))
    (error "Indices out of bound:"
           block-row-index
           block-column-index))
  (let ((row-index (* block-row-index 3))
        (column-index (* block-column-index 3)))
    (members-of-sub-board board
                          row-index
                          (+ row-index 3)
                          column-index
                          (+ column-index 3))))

(define (members-of-row board row-number)
  (members-of-sub-board board
                        row-number
                        (+ row-number 1)
                        0
                        9))

(define (members-of-column board column-number)
  (members-of-sub-board board
                        0
                        9
                        column-number
                        (+ column-number 1)))

(define (sudoku-solved? board)
  (and
   (sudoku-rows-solved? board)
   (sudoku-columns-solved? board)
   (sudoku-blocks-solved? board)))
