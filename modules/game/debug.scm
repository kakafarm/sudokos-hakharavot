(define-module (game debug)
  :pure
  #:export (dp dpp)
  #:use-module (scheme base)
  #:use-module (scheme write)
  #:use-module (dom window console))

(define (dpp . args)
  (let ((port (open-output-string)))
    (display (car args) port)
    (for-each (lambda (x)
                (display " " port)
                (display x port))
              (cdr args))
    (newline port)
    (console:debug (get-output-string port))))

(define-syntax dp
  (syntax-rules ()
    ((dp x)
     (let ((r x))
       (dpp 'x r)
       r))))
