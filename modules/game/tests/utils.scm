(define-module (game tests utils)
  #:pure
  #:export (test-approximate test-equal)
  #:use-module (scheme base)
  )

(define-syntax test-equal
  (syntax-rules ()
    ((test-equal expected test-expr)
     (let ((expected-result expected)
           (test-expr-result test-expr))
       (if (equal? expected-result
                   test-expr-result)
           (list 'PASS
             (list 'test-equal 'expected 'test-expr)
             (list 'result expected-result))
           (list 'FAIL
             (list 'test-equal 'expected 'test-expr)
             (list 'expected-result expected-result)
             (list 'test-expr-result test-expr-result)))))))

(define-syntax test-approximate
  (syntax-rules ()
    ((test-approximate expected test-expr error)
     (let ((expected-result expected)
           (test-expr-result test-expr)
           (error-result error))
       (if (and (>= test-expr-result
                    (- expected-result
                       error-result))
                (<= test-expr-result
                    (+ expected-result
                       error-result)))
           (list 'PASS
             (list 'test-approximate 'expected 'test-expr 'error)
             (list 'expected-result expected-result)
             (list 'error-value error-result))
           (list 'FAIL
             (list 'test-approximate 'expected 'test-expr 'error)
             (list 'expected-result expected-result)
             (list 'test-expr-result test-expr-result)
             (list 'error-value error-result)))))))
