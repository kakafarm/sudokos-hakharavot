(define-module (game draw)
  #:pure
  #:export (draw-number draw-sudoku-board)
  #:use-module (scheme base)
  #:use-module ((hoot syntax) #:select (define*))
  #:use-module (math)
  #:use-module (dom canvas)
  #:use-module (dom window)
  #:use-module (game utils)
  #:use-module (game debug))

(define (draw-number context x y game-size number number-field-size)
  (define font-size-constant (/ 24.0 640.0))
  (define vertical-shift-constant (/ 10.0 640.0))
  (define half-number-field-size (* number-field-size 0.5))
  (set-fill-color! context
                   "#fff")
  (set-font! context
             (string-append "bold "
                            (number->string
                             (* game-size
                                font-size-constant))
                            "px monospace"))
  (set-text-align! context
                   "center")
  (fill-text context
             (number->string number)
             ;; (number->string number)
             (+ x half-number-field-size)
             (+ y
                half-number-field-size
                (* vertical-shift-constant
                   game-size))))

(define (number-field-size game-size border-ratio number-of-columns)
  (lerp (/ game-size number-of-columns)
        0.0
        border-ratio))

(define (calculate-border-size game-size border-size-ratio number-of-columns)
  (let ((number-of-border-columns
         (+ number-of-columns 1)))
    (lerp 0.0
          (/ game-size
             number-of-border-columns)
          border-size-ratio)))

(define* (calculate-number-field-size game-size border-size-ratio number-of-columns)
  (lerp 0.0
        (/ (* 1.0 game-size)
           (* 1.0 number-of-columns))
        (- 1.0 border-size-ratio)))

(define* (draw-number-field #:key context board game-size border-size number-field-size number-of-rows number-of-columns row-index column-index)
  (unless (and context board game-size border-size number-field-size number-of-rows number-of-columns row-index column-index)
    (error "Argument missing."))

  (let* ((t-x (/ (inexact column-index)
                 (- number-of-columns 1)))
         (t-y (inexact (/ row-index
                          (- number-of-rows 1))))
         (to (- game-size
                number-field-size
                border-size))
         (x (lerp border-size
                  to
                  t-x))
         (y (lerp border-size
                  to
                  t-y)))

    ;; Draw number field rectangle.
    (set-fill-color! context "#888")
    (fill-rect context
               x
               y
               number-field-size
               number-field-size)

    (let ((number (vector-ref (vector-ref board row-index)
                              column-index)))
      (when #t ;; XXX: number
        (draw-number context
                     x
                     y
                     game-size
                     1 ;; XXX: number
                     number-field-size)))))

(define (draw-sudoku-board context game-size board)
  (let* ((border-size-ratio 0.1)
         (number-of-rows (exact 9))
         (number-of-columns (exact 9))
         (border-size (calculate-border-size game-size
                                             border-size-ratio
                                             number-of-columns))
         (number-field-size (calculate-number-field-size game-size
                                                         border-size-ratio
                                                         number-of-columns)))
    (do ((row-index 0 (+ row-index 1)))
        ((>= row-index number-of-rows))
      (do ((column-index 0 (+ column-index 1)))
          ((>= column-index number-of-columns))
        (draw-number-field
         #:context context
         #:board board
         #:game-size game-size
         #:border-size border-size
         #:number-field-size number-field-size
         #:number-of-rows number-of-rows
         #:number-of-columns number-of-columns
         #:row-index row-index
         #:column-index column-index)))))
