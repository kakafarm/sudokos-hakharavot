(use-modules
 (guix packages)
 (gnu packages base)
 (gnu packages bash)
 (gnu packages compression)
 (gnu packages entr)
 (gnu packages guile)
 (gnu packages guile-xyz)
 (gnu packages time)
 )

(packages->manifest (list bash coreutils entr findutils gnu-make grep guile-hoot guile-next time zip))
